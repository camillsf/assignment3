import rpg.heroes.*;
import rpg.items.armours.*;
import rpg.items.weapons.*;
import rpg.slots.Slot;

public class Main {

    public static void main(String [] args){
        String ANSI_RED    = "\u001B[31m"; //string to add red colour to the output
        String ANSI_RESET  = "\u001B[0m"; //string to reset output colour.


        //generating some characters
        HeroStrategy w = new Warrior();
        HeroStrategy r = new Ranger();
        HeroStrategy m = new Mage();
        Hero warrior = new Hero(HeroType.WARRIOR,w);
        Hero ranger = new Hero(HeroType.RANGER, r);
        Hero mage = new Hero(HeroType.MAGE, m);

        System.out.println(ANSI_RED + "\nHero stats before leveling up: " + ANSI_RESET);//I add red colour to highlight what is demonstrated
        printHeroStats(warrior);
        printHeroStats(ranger);
        printHeroStats(mage);

        //leveling up by giving the heroes xp
        warrior.gainExperience(100 + 110 + 121 + 133 + 146 + 160 + 176 + 193 + 61);
        ranger.gainExperience(100 + 110 + 121 + 133 + 40);
        mage.gainExperience(100 + 110 + 121 + 133 + 146 + 160 + 176 + 193 + 212 + 233 + 256 + 281 + 309 + 339 + 210);

        System.out.println(ANSI_RED + "\nHero stats after leveling up: " + ANSI_RESET);
        printHeroStats(warrior);
        printHeroStats(ranger);
        printHeroStats(mage);


        //Creating some items
        WeaponStrategy mel = new Melee();
        WeaponStrategy mag = new Magic();
        WeaponStrategy rang = new Ranged();
        Weapon melee = new Weapon("Great Axe of the Exiled", 5, Slot.WEAPON, mel, WeaponType.MELEE);
        Weapon magic = new Weapon("Magic wand", 8, Slot.WEAPON, mag, WeaponType.MAGIC);
        Weapon ranged = new Weapon("Long Bow of the Lone Wolf", 11, Slot.WEAPON, rang, WeaponType.RANGED);
        ArmourStrategy c = new Cloth();
        ArmourStrategy l = new Leather();
        ArmourStrategy p = new Plate();
        Armour clothHead = new Armour("Cloth Wizard Hat", 4, Slot.HEAD, c, ArmourType.CLOTH);
        Armour clothBody = new Armour("Cloth Body of the Magi", 8, Slot.BODY, c, ArmourType.CLOTH);
        Armour clothLegs = new Armour("Cloth Leggings of the Magi", 10, Slot.LEGS, c, ArmourType.CLOTH);
        Armour leatherHead = new Armour("Leather Hat", 7, Slot.HEAD, l, ArmourType.LEATHER);
        Armour leatherBody = new Armour("Leather Jacket", 13, Slot.BODY, l, ArmourType.LEATHER );
        Armour leatherLegs = new Armour("Leather Tights", 3, Slot.LEGS, l, ArmourType.LEATHER);
        Armour plateHead = new Armour("Plate Helmet of the Juggernaut", 15, Slot.HEAD, p, ArmourType.PLATE);
        Armour plateBody = new Armour("Plate Chest of the Juggernaut", 5, Slot.BODY, p, ArmourType.PLATE);
        Armour plateLegs = new Armour("Plate Legs of the Juggernaut", 9, Slot.LEGS, p, ArmourType.PLATE);

        System.out.println(ANSI_RED + "\nItems created: " + ANSI_RESET);
        printWeaponStats(melee);
        printWeaponStats(magic);
        printWeaponStats(ranged);
        printArmorStats(clothHead);
        printArmorStats(clothBody);
        printArmorStats(clothLegs);
        printArmorStats(leatherHead);
        printArmorStats(leatherBody);
        printArmorStats(leatherLegs);
        printArmorStats(plateHead);
        printArmorStats(plateBody);
        printArmorStats(plateLegs);

        System.out.println(ANSI_RED + "\nHero stats before equipping items: " + ANSI_RESET);
        printHeroStats(warrior);
        printHeroStats(ranger);
        printHeroStats(mage);

        //equip items to characters
        warrior.equipItem(leatherHead);
        warrior.equipItem(clothBody);
        warrior.equipItem(plateLegs);
        warrior.equipItem(magic);
        ranger.equipItem(clothHead);
        ranger.equipItem(plateBody);
        ranger.equipItem(leatherLegs);
        ranger.equipItem(melee);
        mage.equipItem(plateHead);
        mage.equipItem(leatherBody);
        mage.equipItem(clothLegs);
        mage.equipItem(ranged);

        System.out.println(ANSI_RED + "\nHero stats after equipping items: " + ANSI_RESET);
        printHeroStats(warrior);
        printHeroStats(ranger);
        printHeroStats(mage);

        //changing equipment
        mage.equipItem(leatherHead);
        mage.equipItem(plateBody);

        System.out.println(ANSI_RED + "\n MAGE stats after changing equipment: " + ANSI_RESET);
        printHeroStats(mage);

        //Heroes attacking
        System.out.println(ANSI_RED + "\nHeroes attacking: " + ANSI_RESET);
        attack(warrior);
        attack(ranger);
        attack(mage);


    }

    static void printHeroStats(Hero hero){
        //method to print heroes stats
        HeroType type = hero.getType();
        System.out.println("\n" + type + " details \n-----------------------------");
        System.out.println("HP: " + hero.getHp());
        System.out.println("Str: " + hero.getStr());
        System.out.println("Dex: " + hero.getDex());
        System.out.println("Int: " + hero.getIntel());
        System.out.println("Level: " + hero.getLevel());
        System.out.println("XP to next level: " + hero.getExperienceToNext());
    }

    static void printWeaponStats(Weapon weapon){
        //method to print weapon stats
        System.out.println("\nItem stats for: " + weapon.getName() + "\n-----------------------------");
        System.out.println("Weapon type: " + weapon.getType());
        System.out.println("Weapon level: " + weapon.getLevel());
        System.out.println("Damage: " + weapon.getDamage());
    }

    static void printArmorStats(Armour armour){
        //method to print armour stats
        System.out.println("\nItem stats for: " + armour.getName() + "\n-----------------------------");
        System.out.println("Armour Type: " + armour.getType());
        System.out.println("Slot: " + armour.getSlot());
        System.out.println("Armour level: " + armour.getLevel());
        if (armour.getBonusHp() != null) {
            System.out.println("Bonus HP: " + armour.getBonusHp());
        }if (armour.getBonusStr() != null){
            System.out.println("Bonus Str: " + armour.getBonusStr());
        }if (armour.getBonusDex() != null){
            System.out.println("Bonus Dex: " + armour.getBonusDex());
        }if(armour.getBonusIntell() != null){
            System.out.println("Bonus Int: " + armour.getBonusIntell());
        }
    }

    static void attack(Hero hero){
        //method to show attack stats (hero stats plus damage dealt)
        printHeroStats(hero);
        System.out.println("\nAttacking for damage: " + hero.getDamage());
    }
}
