package rpg.slots;

public enum Slot {
    WEAPON,
    HEAD,
    BODY,
    LEGS;
}
