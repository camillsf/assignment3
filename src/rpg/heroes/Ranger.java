package rpg.heroes;

import java.util.HashMap;

public class Ranger implements HeroStrategy {
    @Override
    public void generateBaseStats(HashMap<String, Integer> baseStats) {
        //setting the baseStats for Ranger hero's.
        baseStats.put("Health", 120);
        baseStats.put("Strength", 5);
        baseStats.put("Dexterity", 10);
        baseStats.put("Intelligence", 2);
    }

    @Override
    public void levelUpStats(HashMap<String, Integer> baseStats) {
        //increasing the Rangers baseStats when leveling up.
        int hp = baseStats.get("Health");
        baseStats.replace("Health", hp + 20);
        int str = baseStats.get("Strength");
        baseStats.replace("Strength", str + 2);
        int dex = baseStats.get("Dexterity");
        baseStats.replace("Dexterity", dex + 5);
        int intel = baseStats.get("Intelligence");
        baseStats.replace("Intelligence", intel + 1);
    }
}
