package rpg.heroes;

import java.util.HashMap;

public interface HeroStrategy {

    void generateBaseStats(HashMap<String, Integer> baseStats);

    void levelUpStats(HashMap<String, Integer> baseStats);
}
