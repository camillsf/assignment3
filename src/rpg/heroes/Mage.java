package rpg.heroes;

import java.util.HashMap;

public class Mage implements HeroStrategy {
    @Override
    public void generateBaseStats(HashMap<String, Integer> baseStats) {
        //setting Melee hero's baseStats
        baseStats.put("Health", 100);
        baseStats.put("Strength", 2);
        baseStats.put("Dexterity", 3);
        baseStats.put("Intelligence", 10);
    }

    @Override public void levelUpStats(HashMap<String, Integer> baseStats) {
        //increasing the baseStats for Melee's when leveling up
        int hp = baseStats.get("Health");
        baseStats.replace("Health", hp + 15);
        int str = baseStats.get("Strength");
        baseStats.replace("Strength", str + 1);
        int dex = baseStats.get("Dexterity");
        baseStats.replace("Dexterity", dex + 2);
        int intel = baseStats.get("Intelligence");
        baseStats.replace("Intelligence", intel + 5);
    }
}
