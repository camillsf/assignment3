package rpg.heroes;

import java.util.HashMap;

public class Warrior implements HeroStrategy {
    @Override
    public void generateBaseStats(HashMap<String, Integer> baseStats) {
        //setting teh baseStats for Warrior hero's.
        baseStats.put("Health", 150);
        baseStats.put("Strength", 10);
        baseStats.put("Dexterity", 3);
        baseStats.put("Intelligence", 1);
    }

    @Override
    public void levelUpStats(HashMap<String, Integer> baseStats) {
        //increasing the baseStats when Warriors level up.
        int hp = baseStats.get("Health");
        baseStats.replace("Health", hp + 30);
        int str = baseStats.get("Strength");
        baseStats.replace("Strength", str + 5);
        int dex = baseStats.get("Dexterity");
        baseStats.replace("Dexterity", dex + 2);
        int intel = baseStats.get("Intelligence");
        baseStats.replace("Intelligence", intel + 1);

    }
}
