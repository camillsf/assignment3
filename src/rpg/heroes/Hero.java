package rpg.heroes;

import rpg.slots.Slot;
import rpg.items.Item;
import rpg.items.armours.Armour;
import rpg.items.weapons.Weapon;
import rpg.items.weapons.WeaponType;
import java.util.HashMap;

public class Hero {
    private HeroType type;
    private HeroStrategy strategy;
    private HashMap<String, Integer> baseStats;
    private HashMap<String, Integer> bonusStatsHead;
    private HashMap<String, Integer> bonusStatsBody;
    private HashMap<String, Integer> bonusStatsLegs;
    private int level;
    private int damage;
    private int experience;
    private int experienceThreshold;
    private HashMap<Slot, Item> slots;

    public Hero(HeroType type, HeroStrategy strategy){
        this.type = type;
        this.strategy = strategy;
        level = 1;
        damage = 0;
        experience = 0;
        experienceThreshold = 100;
        slots = new HashMap<Slot, Item>(); //hashMap containing the items in the different slots.
        baseStats = new HashMap<String, Integer>();  //hashMap containing the baseStats for the Hero
        bonusStatsHead = new HashMap<String, Integer>();  //hashMap containing the bonusStats from head-armour
        bonusStatsBody = new HashMap<String, Integer>(); //hashMap containing the bonusStats from body-armour
        bonusStatsLegs = new HashMap<String, Integer>();  //hashMap containing the bonusStats from legs-armour
        AddBaseStats(); //call on method AddBaseStats to set the Hero's base stats.
    }


    private void AddBaseStats(){
        //Method to generate baseStats for each Hero
        strategy.generateBaseStats(baseStats); //parsing baseStats HashMap as parameter to the strategy's method to generate baseStats.
    }


    public void gainExperience(int gainedExperience){
        //Takes an integer of gained experience and increases the xp.
        // If the xp gained is so much that the Hero should level up, this method will call on the levelUp method.
        experience = experience + gainedExperience; //setting experience to the current xp plus the gained xp.
        while (experience >= experienceThreshold){ //while loop tp keep checking if experience has reached the experience threshold and the hero should level up.
            levelUp(); //calling the level up method.
            experience = experience - experienceThreshold; //setting the experience to the current experience minus the threshold.
            experienceThreshold = experienceThreshold + (int)(experienceThreshold * 0.1); //setting the new experience threshold to level up, this increases with ten percent each time.
        }
    }


    private void levelUp(){
        //Method to level up the hero.
        level = level + 1; //increasing the level by one.
        strategy.levelUpStats(baseStats); //parsing the baseStats HashMap parameter to the the strategy's levelUpStats method to increase the stats when leveling up.
    }


    public void equipItem(Item item){
        //Takes an item an checks if the hero is able to equip it, if he/she is we add the item to its slot.
        // Also checks if the item is a weapon or a armour and calls on the respective equip methods.
        int weaponLevel = item.getLevel();
        if (weaponLevel <= level){ //checking if the Hero can equip the Item, level needs to be the same or greater than the weapons level.
            Slot slot = item.getSlot();
            if (item instanceof Weapon){ //check if we are equipping a weapon
                Weapon weapon = (Weapon) item; // turn item into weapon
                if (slots.containsKey(slot)){
                    slots.replace(slot, weapon); //if slot already exists we replace weapon in the slot.
                }else{
                    slots.put(slot, weapon); // or else we add weapon slot with new weapon.
                }
                equipWeapon(weapon); //call on method equip weapon with weapon as parameter.
            }else if (item instanceof Armour){ //check if we are equipping an armour.
                Armour armour = (Armour) item; //turn item into armour
                if (slots.containsKey(slot)){
                    slots.replace(slot, armour); //if slot already exists we replace the armour in the slot.
                }else{
                    slots.put(slot, armour); //or else we add the slot with the armour.
                }
                equipArmour(armour); //call on method to equip armour with armour as parameter.
            }
        }else { //if the hero can't equip the item we print an error message
            System.out.println("Error. The Hero is not at a high enough level to equip this item.");
        }
    }


    private void equipWeapon(Weapon weapon){
        //Takes a weapon and check what type it is, before calling on the respective method to equip that type.
        WeaponType type = weapon.getType(); //get weapon type
        int weaponDamage = weapon.getDamage(); //get weapons damage
        if (type == WeaponType.MELEE){ //if we are equipping a melee weapon
            equipMeleeWeapon(weaponDamage); //call on equipMeleeWeapon with the weaponDamage as the parameter
        }else if (type == WeaponType.MAGIC){ //if we are equipping a magic weapon
            equipMagicWeapon(weaponDamage); //call on equipMagicWeapon with the weaponDamage as the parameter
        }else if (type == WeaponType.RANGED){ //if we are equipping a ranged weapon
            equipRangedWeapon(weaponDamage); //call on equipRangedWeapon with the weaponDamage as the parameter
        }
    }


    private void equipMeleeWeapon(int weaponDamage){
        //takes an integer of weaponDamage, and calculates the Hero's damage with this weapon;
        int effectiveStr = getStr(); //get heroes effective strength
        damage = weaponDamage + (int) (effectiveStr * 1.5); //set Hero's damage to weaponDamage plus the effective strength multiplied with 1.5 rounded down.
    }


    private void equipMagicWeapon(int weaponDamage){
        //takes an integer of weaponDamage, and calculates the Hero's damage with this weapon;
        int effectiveIntel = getIntel(); //get Hero's effective Intelligence
        damage = weaponDamage + (int) (effectiveIntel * 3);//set Hero's damage to weaponDamage plus the effective Intelligence multiplied with 3 rounded down.
    }


    private void equipRangedWeapon(int weaponDamage){
        //takes an integer of weaponDamage, and calculates the Hero's damage with this weapon;
        int effectiveDex = getDex(); //get Hero's effective Dexterity
        damage = weaponDamage + (int) (effectiveDex * 2); //set Hero's damage to weaponDamage plus the effective Dexterity multiplied with 2 rounded down.

    }


    private void equipArmour(Armour armour){
        //Takes an armour and updates the bonusStats of the type of armour the hero is equipping.
        Slot slot = armour.getSlot(); //get armour-Slot
        if (slot == Slot.HEAD){ //if head-slot
            bonusStatsHead.clear();
            bonusStatsHead = armour.getStatBonuses(); //update bonusStats for head
        }else if (slot == Slot.BODY){ //if body-slot
            bonusStatsBody.clear();
            bonusStatsBody = armour.getStatBonuses(); //update bonusStats for body
        }else if (slot == Slot.LEGS){ //if legs slot
            bonusStatsLegs.clear();
            bonusStatsLegs = armour.getStatBonuses(); //update bonusStats for legs
        }
    }


    //getters
    public HeroType getType(){
        return  type;
    }


    public  int getLevel(){
        return level;
    }


    public int getDamage(){
        return damage;
    }


    public int getExperienceToNext(){
        return experienceThreshold - experience; //returning how much Xp the Hero needs to gain to level up.
    }


    public int getHp(){
        int hp = baseStats.get("Health"); //getting Hero's base-stats health
        if (! bonusStatsHead.isEmpty()){ //checking if there are bonusStats for head
            if (bonusStatsHead.containsKey("Health")){ //checking if the bonusStats contains Health
                hp = hp + bonusStatsHead.get("Health"); //adding base-stats hp with bonus stats hp to get effective hp.
            }
        }if (! bonusStatsBody.isEmpty()){//checking if there are bonusStats for body
            if (bonusStatsBody.containsKey("Health")){ //checking if the bonusStats contains Health
                hp = hp + bonusStatsBody.get("Health"); //adding base-stats hp with bonus stats hp to get effective hp.
            }
        }if (! bonusStatsLegs.isEmpty()){ //checking if there are bonusStats for legs
            if (bonusStatsLegs.containsKey("Health")){ //checking if the bonusStats contains Health
                hp = hp + bonusStatsLegs.get("Health"); //adding base-stats hp with bonus stats hp to get effective hp.
            }
        }
        return hp; //returning effective Hp
    }


    public int getStr(){
        //this method does the same as getHP() method, just with Strength instead of Health
        int str = baseStats.get("Strength");
        if (! bonusStatsHead.isEmpty()){
            if (bonusStatsHead.containsKey("Strength")){
                str = str + bonusStatsHead.get("Strength");
            }
        }if (! bonusStatsBody.isEmpty()){
            if (bonusStatsBody.containsKey("Strength")){
                str = str + bonusStatsBody.get("Strength");
            }
        }if (! bonusStatsLegs.isEmpty()){
            if (bonusStatsLegs.containsKey("Strength")){
                str = str + bonusStatsLegs.get("Strength");
            }
        }
        return str;
    }


    public int getDex(){
        //this method does the same as getHP() method, just with Dexterity instead of Health
        int dex = baseStats.get("Dexterity");
        if (! bonusStatsHead.isEmpty()){
            if (bonusStatsHead.containsKey("Dexterity")){
                dex = dex + bonusStatsHead.get("Dexterity");
            }
        }if (! bonusStatsBody.isEmpty()){
            if (bonusStatsBody.containsKey("Dexterity")){
                dex = dex + bonusStatsBody.get("Dexterity");
            }
        }if (! bonusStatsLegs.isEmpty()){
            if (bonusStatsLegs.containsKey("Dexterity")){
                dex = dex + bonusStatsLegs.get("Dexterity");
            }
        }
        return dex;
    }


    public int getIntel(){
        //this method does the same as getHP() method, just with Intelligence instead of Health
        int intel = baseStats.get("Intelligence");
        if (! bonusStatsHead.isEmpty()){
            if (bonusStatsHead.containsKey("Intelligence")){
                intel = intel + bonusStatsHead.get("Intelligence");
            }
        }if (! bonusStatsBody.isEmpty()){
            if (bonusStatsBody.containsKey("Intelligence")){
                intel = intel + bonusStatsBody.get("Intelligence");
            }
        }if (! bonusStatsLegs.isEmpty()){
            if (bonusStatsLegs.containsKey("Intelligence")){
                intel = intel + bonusStatsLegs.get("Intelligence");
            }
        }
        return intel;
    }


    public Weapon getWeapon(){
        if (!slots.isEmpty()){ // if there are slots in the slots Hashmap
            if (slots.containsKey(Slot.WEAPON)){ //if the slots contain the weapon Slot
                return (Weapon) slots.get(Slot.WEAPON); //return the weapon in the slot
            }
        }
        return null; //if there are no slots in the slots HasMap or the slot don't contain the weapon slot - return null
    }


    public Armour getHeadArmour(){
        //this method does the same as getWeapon, just with head slot instead of weapon Slot
        if (!slots.isEmpty()){
            if (slots.containsKey(Slot.HEAD)){
                return (Armour) slots.get(Slot.HEAD);
            }
        }
        return null;
    }


    public Armour getBodyArmour(){
        //this method does the same as getWeapon, just with body slot instead of weapon Slot
        if (!slots.isEmpty()){
            if (slots.containsKey(Slot.BODY)){
                return (Armour) slots.get(Slot.BODY);
            }
        }
        return null;
    }


    public Armour getLegsArmour(){
        //this method does the same as getWeapon, just with legs slot instead of weapon Slot
        if (!slots.isEmpty()){
            if (slots.containsKey(Slot.LEGS)){
                return (Armour) slots.get(Slot.LEGS);
            }
        }
        return null;
    }

}
