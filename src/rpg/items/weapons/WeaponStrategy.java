package rpg.items.weapons;

public interface WeaponStrategy {
    int setDamage(int level);
}
