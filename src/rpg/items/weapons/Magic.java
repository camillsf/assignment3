package rpg.items.weapons;

public class Magic implements WeaponStrategy{

    @Override
    public int setDamage(int level) {
        //takes in an integer representing level and calculates and returns the damage for this specific weapon.
        return 25 + (2 * (level-1));

    }
}
