package rpg.items.weapons;

public enum WeaponType {
    MAGIC,
    MELEE,
    RANGED;
}
