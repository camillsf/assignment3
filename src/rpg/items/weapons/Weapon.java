package rpg.items.weapons;

import rpg.slots.Slot;
import rpg.items.Item;

public class Weapon extends Item {
    private WeaponStrategy strategy;
    private WeaponType type;
    private int damage;

    public Weapon(String name, int level, Slot slot, WeaponStrategy strategy, WeaponType type){
        super(name, level, slot); //call on superclass constructor with name, level and slot
        this.strategy = strategy;
        this.type = type;
        damage = 0; //initialise damage
        setDamage(); //call on method to set weapons damage

    }

    private void setDamage() {
        //method to generate the weapons damage depending of weapon type.
        damage = strategy.setDamage(level); //setting damage to the return value of the relevant strategy's setDamage method.
    }


    //getters
    public int getDamage(){
        return damage;
    }


    public WeaponType getType(){
        return type;
    }

}
