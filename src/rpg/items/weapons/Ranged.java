package rpg.items.weapons;

public class Ranged implements WeaponStrategy{

    @Override
    public int setDamage(int level) {
        //takes in an integer representing level and calculates and returns the damage for this specific weapon.
        return 5 + (3 * (level-1));

    }
}
