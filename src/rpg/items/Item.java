package rpg.items;

import rpg.slots.Slot;

public abstract class Item {
    public String name;
    public int level;
    public Slot slot;


    public Item(String name, int level, Slot slot){ //all items must have a name, a level and a Slot.
        this.name = name;
        this.level = level;
        this.slot = slot;
    }

    //getters
    public String getName(){
        return name;
    }


    public int getLevel(){
        return level;
    }


    public Slot getSlot(){
        return slot;
    }
}
