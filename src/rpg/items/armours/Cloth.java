package rpg.items.armours;

import rpg.slots.Slot;

import java.util.HashMap;

public class Cloth implements ArmourStrategy{

    @Override
    public void setStatBonuses(HashMap<String, Integer> statBonuses, int level, double scaler) {
        //setting statBonuses for Cloth Armour
        int hp = (int) ((10 + (5 * (level-1))) * scaler); //calculate health
        int dex = (int) ((1 + (level-1)) * scaler); //calculate dexterity
        int intell = (int) ((3 + (2 * (level-1))) *scaler); //calculate intelligence

        //add health, dexterity and intelligence to statBonuses
        statBonuses.put("Health", hp);
        statBonuses.put("Dexterity", dex);
        statBonuses.put("Intelligence", intell);

    }
}
