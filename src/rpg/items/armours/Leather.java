package rpg.items.armours;

import rpg.slots.Slot;

import java.util.HashMap;

public class Leather implements ArmourStrategy{

    @Override
    public void setStatBonuses(HashMap<String, Integer> statBonuses, int level, double scaler) {
        //setting statBonuses for Leather Armour
        int hp = (int) ((20 + (8 * (level-1))) * scaler); //calculate health
        int str = (int) ((1 + (level-1)) *scaler); //calculate strength
        int dex = (int) ((3 + (2 * (level-1))) *scaler); //calculate dexterity

        //add healt, strength and dexterity to statBonuses
        statBonuses.put("Health", hp);
        statBonuses.put("Strength", str);
        statBonuses.put("Dexterity", dex);

    }
}
