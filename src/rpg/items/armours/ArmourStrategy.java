package rpg.items.armours;

import rpg.slots.Slot;

import java.util.HashMap;

public interface ArmourStrategy {

    void setStatBonuses(HashMap<String, Integer> statBonuses, int level, double scaler);
}
