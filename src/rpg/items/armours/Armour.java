package rpg.items.armours;

import rpg.slots.Slot;
import rpg.items.Item;
import java.util.HashMap;

public class Armour extends Item {
    private ArmourStrategy strategy;
    private ArmourType type;
    private HashMap<String, Integer> statBonuses;


    public Armour(String name, int level, Slot slot, ArmourStrategy strategy, ArmourType type){
        super(name, level, slot); //call on superclass constructor with name, level and slot
        this.strategy = strategy;
        this.type = type;
        statBonuses = new HashMap<String, Integer>(); //hashMap containing StatBonuses
        addStatBonuses(); //call on method to set StatBonuses
    }


    private void addStatBonuses(){
        //Method to generate statBonuses for each Armour depending on armourType and Slot.
        double scaler = 0;
        if (slot == Slot.HEAD){
            scaler = 0.80; //if slot is head the scaler is 0.80
        }else if (slot == Slot.BODY){
            scaler = 1.00; //if slot is body the scaler is 1.00
        }else if(slot == Slot.LEGS){
            scaler = 0.60; //if slot is legs the scaler is 0.60
        }
        strategy.setStatBonuses(statBonuses, level, scaler); //parsing statBonuses HashMap, level and scaler as parameter to the strategy's method to generate statBonuses.
    }

    //getters
    public ArmourType getType() {
        return type;
    }


    public HashMap<String,Integer> getStatBonuses(){
        return statBonuses;
    }


    //these getters are just used for testing purposes
    public java.lang.Integer getBonusHp(){
        return statBonuses.get("Health");
    }

    public java.lang.Integer getBonusStr(){
        return statBonuses.get("Strength");
    }

    public java.lang.Integer getBonusDex(){
        return statBonuses.get("Dexterity");
    }

    public java.lang.Integer getBonusIntell(){
        return statBonuses.get("Intelligence");
    }
}
