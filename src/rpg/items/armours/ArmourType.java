package rpg.items.armours;

public enum ArmourType {
    CLOTH,
    LEATHER,
    PLATE;
}
