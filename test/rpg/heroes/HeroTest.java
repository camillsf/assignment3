package rpg.heroes;

import org.junit.jupiter.api.Test;
import rpg.items.armours.*;
import rpg.items.weapons.*;
import rpg.slots.Slot;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    private final HeroStrategy w = new Warrior();
    private final HeroStrategy r = new Ranger();
    private final HeroStrategy m = new Mage();

    private final Hero warrior = new Hero(HeroType.WARRIOR, w);
    private final Hero ranger = new Hero(HeroType.RANGER, r);
    private final Hero mage = new Hero(HeroType.MAGE, m);

    //Testing that warriors start stats are correct
    @Test
    void checkWarriorLevel(){
        assertEquals(1, warrior.getLevel());
    }

    @Test
    void checkWarriorHp(){
        assertEquals(150, warrior.getHp());
    }

    @Test
    void checkWarriorStr(){
        assertEquals(10, warrior.getStr());
    }

    @Test
    void checkWarriorDex(){
        assertEquals(3, warrior.getDex());
    }

    @Test
    void checkWarriorIntell(){
        assertEquals(1, warrior.getIntel());
    }


    //testing that rangers start-stats are coorect
    @Test
    void checkRangerLevel(){
        assertEquals(1, ranger.getLevel());
    }

    @Test
    void checkRangerHp(){
        assertEquals(120, ranger.getHp());
    }

    @Test
    void checkRangerStr(){
        assertEquals(5, ranger.getStr());
    }

    @Test
    void checkRangerDex(){
        assertEquals(10, ranger.getDex());
    }

    @Test
    void checkRangerIntell(){
        assertEquals(2, ranger.getIntel());
    }


    //testing that mages start with the correct stats
    @Test
    void checkMageLevel(){
        assertEquals(1, mage.getLevel());
    }

    @Test
    void checkMageHp(){
        assertEquals(100, mage.getHp());
    }

    @Test
    void checkMageStr(){
        assertEquals(2, mage.getStr());
    }

    @Test
    void checkMageDex(){
        assertEquals(3, mage.getDex());
    }

    @Test
    void checkMageIntell(){
        assertEquals(10, mage.getIntel());
    }


    //testing that warrior gets correct stats after level up
    @Test
    void checkWarriorLevelUp(){
        warrior.gainExperience(100);
        assertEquals(2, warrior.getLevel());
    }

    @Test
    void checkWarriorLevelUpHp(){
        warrior.gainExperience(100);
        assertEquals(180, warrior.getHp());
    }

    @Test
    void checkWarriorLevelUpStr(){
        warrior.gainExperience(100);
        assertEquals(15, warrior.getStr());
    }

    @Test
    void checkWarriorLevelUpDex(){
        warrior.gainExperience(100);
        assertEquals(5, warrior.getDex());
    }

    @Test
    void checkWarriorLevelUpIntell(){
        warrior.gainExperience(100);
        assertEquals(2, warrior.getIntel());
    }


    //testing that rangers gets correct stats after level up
    @Test
    void checkRangerLevelUp(){
        ranger.gainExperience(100);
        assertEquals(2, ranger.getLevel());
    }

    @Test
    void checkRangerLevelUpHp(){
        ranger.gainExperience(100);
        assertEquals(140, ranger.getHp());
    }

    @Test
    void checkRangerLevelUpStr(){
        ranger.gainExperience(100);
        assertEquals(7, ranger.getStr());
    }

    @Test
    void checkRangerLevelUpDex(){
        ranger.gainExperience(100);
        assertEquals(15, ranger.getDex());
    }

    @Test
    void checkRangerLevelUpIntell(){
        ranger.gainExperience(100);
        assertEquals(3, ranger.getIntel());
    }


    //testing that mages gets correct stats after level up
    @Test
    void checkMageLevelUp(){
        mage.gainExperience(100);
        assertEquals(2, mage.getLevel());
    }

    @Test
    void checkMageLevelUpHp(){
        mage.gainExperience(100);
        assertEquals(115, mage.getHp());
    }

    @Test
    void checkMageLevelUpStr(){
        mage.gainExperience(100);
        assertEquals(3, mage.getStr());
    }

    @Test
    void checkMageLevelUpDex(){
        mage.gainExperience(100);
        assertEquals(5, mage.getDex());
    }

    @Test
    void checkMageLevelUpIntell(){
        mage.gainExperience(100);
        assertEquals(15, mage.getIntel());
    }

    //testing level up multiple levels
    @Test
    void checkMultipleLevelUps(){
        warrior.gainExperience(100 + 110 + 121);
        assertEquals(4,warrior.getLevel());
    }

    @Test
    void checkMultipleLevelUpsHp(){
        warrior.gainExperience(100 + 110 + 121 + 133);
        assertEquals(270, warrior.getHp());
    }

    @Test
    void checkMultipleLevelUpsStr(){
        warrior.gainExperience(100 + 110 + 121 + 133 + 146 + 160 + 176 + 193);
        assertEquals(50, warrior.getStr());
    }

    @Test
    void checkMultipleLevelUpsDex(){
        warrior.gainExperience(100 + 110 + 121);
        assertEquals(9, warrior.getDex());
    }

    @Test
    void checkMultipleLevelUpsIntel(){
        warrior.gainExperience(100 + 110 + 121 + 133 + 146 + 160);
        assertEquals(7, warrior.getIntel());
    }


    //make weapons to test heroes equipping weapons
    WeaponStrategy mag = new Magic();
    WeaponStrategy mel = new Melee();
    WeaponStrategy rang = new Ranged();

    Weapon magic = new Weapon("Magic wand", 10, Slot.WEAPON, mag, WeaponType.MAGIC);
    Weapon melee = new Weapon("Great Axe of the Exiled", 5, Slot.WEAPON, mel, WeaponType.MELEE);
    Weapon ranged = new Weapon("Long Bow of the Lone Wolf", 10, Slot.WEAPON, rang, WeaponType.RANGED);


    @Test
    void checkUnEquippedDamage(){
        assertEquals(0, ranger.getDamage());
    }

    @Test
    void checkUnequippedWeapon(){
        assertNull(ranger.getWeapon());
    }

    @Test
    void checkEquipWeaponWithHigherLevel(){
        ranger.equipItem(ranged);
        assertNull(ranger.getWeapon());
    }

    @Test
    void checkEquipRangedWeapon(){
        ranger.gainExperience(100 + 110 + 121 + 133 + 146 + 160 + 176 + 193 + 212);
        ranger.equipItem(ranged);
        assertEquals(ranged, ranger.getWeapon());
    }

    @Test
    void checkDamageEquipRangedWeapon(){
        ranger.gainExperience(100 + 110 + 121 + 133 + 146 + 160 + 176 + 193 + 212);
        ranger.equipItem(ranged);
        assertEquals(142, ranger.getDamage());
    }

    @Test
    void checkEquipMagicWeapon(){
        ranger.gainExperience(100 + 110 + 121 + 133 + 146 + 160 + 176 + 193 + 212);
        ranger.equipItem(magic);
        assertEquals(magic, ranger.getWeapon());
    }

    @Test
    void checkDamageEquipMagicWeapon(){
        ranger.gainExperience(100 + 110 + 121 + 133 + 146 + 160 + 176 + 193 + 212);
        ranger.equipItem(magic);
        assertEquals(76, ranger.getDamage());
    }

    @Test
    void checkEquipMeleeWeapon(){
        ranger.gainExperience(100 + 110 + 121 + 133);
        ranger.equipItem(melee);
        assertEquals(melee, ranger.getWeapon());
    }

    @Test
    void checkDamageEquipMeleeWeapon(){
        ranger.gainExperience(100 + 110 + 121 + 133);
        ranger.equipItem(melee);
        assertEquals(42, ranger.getDamage());
    }


    //making armours to se heroes equipping of armour
    ArmourStrategy c= new Cloth();
    ArmourStrategy l = new Leather();
    ArmourStrategy p = new Plate();

    Armour cloth = new Armour("Cloth Leggings of the Magi", 10, Slot.LEGS, c, ArmourType.CLOTH);
    Armour leather = new Armour("Leather hat", 5, Slot.HEAD, l, ArmourType.LEATHER);
    Armour plate = new Armour("Plate Chest of the Juggernaut", 15, Slot.BODY, p, ArmourType.PLATE);

    @Test
    void checkEquipArmourWthHigherLevel(){
        mage.equipItem(cloth);
        assertNull(mage.getLegsArmour());
    }

    @Test
    void checkEquipCloth(){
        mage.gainExperience(100 + 110 + 121 + 133 + 146 + 160 + 176 + 193 + 212);
        mage.equipItem(cloth);
        assertEquals(cloth, mage.getLegsArmour());
    }

    @Test
    void checkEquipLeather(){
        mage.gainExperience(100 + 110 + 121 + 133);
        mage.equipItem(leather);
        assertEquals(leather, mage.getHeadArmour());
    }

    @Test
    void checkEquipPlate(){
        mage.gainExperience(100 + 110 + 121 + 133 + 146 + 160 + 176 + 193 + 212 + 233 + 256 + 281 + 309 + 339);
        mage.equipItem(plate);
        assertEquals(plate, mage.getBodyArmour());
    }




}