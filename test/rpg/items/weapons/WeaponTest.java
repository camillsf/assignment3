package rpg.items.weapons;

import org.junit.jupiter.api.Test;
import rpg.slots.Slot;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    WeaponStrategy mag = new Magic();
    WeaponStrategy mel = new Melee();
    WeaponStrategy r = new Ranged();

    Weapon magic = new Weapon("Magic wand", 10, Slot.WEAPON, mag, WeaponType.MAGIC);
    Weapon melee = new Weapon("Great Axe of the Exiled", 5, Slot.WEAPON, mel, WeaponType.MELEE);
    Weapon ranged = new Weapon("Long Bow of the Lone Wolf", 10, Slot.WEAPON, r, WeaponType.RANGED);

    //Testing that magic weapon gets created with the correct damage
    @Test
    void checkMagicDamage(){
        assertEquals( 43, magic.getDamage());
    }

    @Test
    void checkMeleeDamage(){
        assertEquals( 23, melee.getDamage());
    }

    @Test
    void checkRangedDamage(){
        assertEquals( 32, ranged.getDamage());
    }
}