package rpg.items.armours;

import org.junit.jupiter.api.Test;
import rpg.slots.Slot;

import static org.junit.jupiter.api.Assertions.*;

class ArmourTest {
    ArmourStrategy c= new Cloth();
    ArmourStrategy l = new Leather();
    ArmourStrategy p = new Plate();

    Armour cloth = new Armour("Cloth Leggings of the Magi", 10, Slot.LEGS, c, ArmourType.CLOTH);
    Armour leather = new Armour("Leather hat", 5, Slot.HEAD, l, ArmourType.LEATHER);
    Armour plate = new Armour("Plate Chest of the Juggernaut", 15, Slot.BODY, p, ArmourType.PLATE);

    //Testing that cloth armour gets made with the correct stats
    @Test
    void checkClothType(){
        assertEquals(ArmourType.CLOTH, cloth.getType());
    }

    @Test
    void checkClothName(){
        assertEquals("Cloth Leggings of the Magi", cloth.getName());
    }

    @Test
    void checkClothLevel(){
        assertEquals(10, cloth.getLevel());
    }

    @Test
    void checkClothSlot(){
        assertEquals(Slot.LEGS, cloth.getSlot());
    }

    @Test
    void checkClothBonusHp(){
        assertEquals(33, cloth.getBonusHp());
    }

    @Test
    void checkClothBonusStr(){
        assertNull(cloth.getBonusStr());
    }

    @Test
    void checkClothBonusDex(){
        assertEquals(6, cloth.getBonusDex());
    }

    @Test
    void checkClothBonusInt(){
        assertEquals(12, cloth.getBonusIntell());
    }


    //Testing that leather armour gets made with the correct stats
    @Test
    void checkLeatherType(){
        assertEquals(ArmourType.LEATHER, leather.getType());
    }

    @Test
    void checkLeatherName(){
        assertEquals("Leather hat", leather.getName());
    }

    @Test
    void checkLeatherLevel(){
        assertEquals(5, leather.getLevel());
    }

    @Test
    void checkLeatherSlot(){
        assertEquals(Slot.HEAD, leather.getSlot());
    }

    @Test
    void checkLeatherBonusHp(){
        assertEquals(41, leather.getBonusHp());
    }

    @Test
    void checkLeatherBonusStr(){
        assertEquals(4, leather.getBonusStr());
    }

    @Test
    void checkLeatherBonusDex(){
        assertEquals(8, leather.getBonusDex());
    }

    @Test
    void checkLeatherBonusInt(){
        assertNull(leather.getBonusIntell());
    }



    //Testing that plate armour gets made with the correct stats
    @Test
    void checkPlateType(){
        assertEquals(ArmourType.PLATE, plate.getType());
    }

    @Test
    void checkPlateName(){
        assertEquals("Plate Chest of the Juggernaut", plate.getName());
    }

    @Test
    void checkPlateLevel(){
        assertEquals(15, plate.getLevel());
    }

    @Test
    void checkPlateSlot(){
        assertEquals(Slot.BODY, plate.getSlot());
    }

    @Test
    void checkPlateBonusHp(){
        assertEquals(198, plate.getBonusHp());
    }

    @Test
    void checkPlateBonusStr(){
        assertEquals(31, plate.getBonusStr());
    }

    @Test
    void checkPlateBonusDex(){
        assertEquals(15, plate.getBonusDex());
    }

    @Test
    void checkPlateBonusInt(){
        assertNull(plate.getBonusIntell());
    }

}