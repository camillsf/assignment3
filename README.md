# Assignment 3

This project is a RPG *Role playing game* character system. 
It simulates the characters/heroes and the items they interact with, which are weapons and armours. 

The system consists of three types of characters/heroes, three types of weapons and three types of armours. A character/hero can equip one weapon and three armours at the same time, but the three armours must be for each bodypart; the head, the body and the legs. When a character/hero levels up or equips an armour, the characters/hero's stats gets affected. When a character/hero attacks the damage dealt is affected by the weapon the character/hero carries. 

To run a demonstration of the system go to the Assignment3 folder and run 
```
java -jar assignment3.jar
```

The demonstrations shows: 
- Generation of characters 
- Demonstrating that Leveling up heroes leads to increased attributes/stats
- Creation of items 
- Equipping items to characters, demonstrates that characters effective stats are altered
- Changing characters equipment, demonstrates that attributes arent infinitly increased
- Displaying damage dealt when characters are attacking 

